#!/usr/bin/python

## IMPORTING PACKAGES
import sys
from netCDF4 import Dataset
import numpy as np

## READING INPUT-FILE
ifile=sys.argv[1]

## CREATING POINTFILE			
pointfile_name = "tmp_pointfile.dat"
pointfile_id = open(pointfile_name, 'w+')

## LOADING DATA FROM NETCDF-file
nc_fid = Dataset(ifile,'r');
lon = np.array(nc_fid.variables['lon_40hz'][:])
lat = np.array(nc_fid.variables['lat_40hz'][:])
swh = np.array(nc_fid.variables['swh_40hz'][:]) #Significant weight height used as arbitrary height for Geoip/RAVSOFT

## RESHAPE into columns
lon = np.reshape(lon,lon.size) 
lat = np.reshape(lat,lon.size)
swh = np.reshape(swh,lon.size)	
idn = np.array(range(1, len(lon)+1))		

## LATITUDES larger than 90 set to -90, thus ignored by GRAVSOFT		
lat[lat > 90] = -90;	

## MAKE and SAVE pointfile
data = np.array([idn, lat, lon, swh])
data = data.T
np.savetxt(pointfile_id, data,fmt='%d %3.6f %3.6f %4.8f', delimiter='\t')

pointfile_id.close()
	
