#!/usr/bin/python

## IMPORTING PACKAGES
import sys
from netCDF4 import Dataset
from shutil import copyfile
import numpy as np

## READ FILE FROM STDIN
file_str=sys.argv[1]

## Open NC-file for reading and editing
nc_fid = Dataset(file_str,'r+');

## CREATE ARRAYS FOR 1 and 40 hz
length = len(nc_fid.dimensions["time"]);
array_1hz = np.arange(0, length, dtype='int16')
array_40hz = np.reshape(np.repeat(array_1hz,40),[length, 40])

## READ NEEDED VARIABLES for calculations
lat_40hz = np.array(nc_fid.variables['lat_40hz'][:])
alt_40hz = np.array(nc_fid.variables['alt_40hz'][:])	
range_40hz = np.array(nc_fid.variables['range_40hz'][:])
mss = np.array(nc_fid.variables['mean_sea_surface'][:])
ssha = np.array(nc_fid.variables['ssha'][:])

## CREATE OCEAN INDEX
ocean_idx = np.array(nc_fid.variables['surface_type'][:])
ocean_idx = ocean_idx.astype(float)
ocean_idx[ocean_idx != 0] = np.nan
ocean_idx[ocean_idx == 0] = 1
ocean_idx_40hz = np.reshape(np.repeat(ocean_idx,40),[length,40])

#Making index for good and bad values
idxs = array_40hz*0+1
points = np.loadtxt("tmp_pointfile.dat")
points_40hz = np.reshape(points[:,2], [length,40])
ok_idx = (points_40hz<500).astype(int)
bad_idx = (points_40hz>500).astype(int)

# Load EGM08 and DTU15
EGM_str = "intpEGM08.dat"
DTU15_str = "intpDTU15.dat"

EGMmat = array_40hz*0 #allocating space for EGM08 dats
DTUmat = array_40hz*0 #allocating space for DTU15 data	
	
EGM08 = np.reshape(np.loadtxt(EGM_str), [length,40])
DTU15 = np.reshape(np.loadtxt(DTU15_str), [length,40])

EGM08[ok_idx==0]=np.nan
DTU15[ok_idx==0]=np.nan

# Remove land-data by multiplying with ocean_idx

sshao = np.multiply(ssha,ocean_idx)
msso = np.multiply(mss,ocean_idx)
		
alto = np.multiply(alt_40hz,ocean_idx_40hz)
rangeo = np.multiply(range_40hz,ocean_idx_40hz) 
EGM08o = np.multiply(EGM08,ocean_idx_40hz)
DTU15o = np.multiply(DTU15,ocean_idx_40hz)

# Deflection of the vertical (aka sea surface slope) calculation		
deflection = alto - rangeo + EGM08o;
deflection_slope = np.array(deflection[:,2:-1]-deflection[:,0:-3], dtype=float);
sea_surface_slope = array_40hz  + np.nan;	
sea_surface_slope[:,1:38] = deflection_slope

# Removing outliers		
sea_surface_slope[sea_surface_slope>0.6] = np.nan;
sea_surface_slope[sea_surface_slope<-0.6] = np.nan;

# Removing bad values
DTU15[bad_idx]=np.nan
EGM08[bad_idx]=np.nan
sea_surface_slope[bad_idx]=np.nan

# Saving into temporary ascii-files
np.savetxt('tmp_DTU15.dat',DTU15)
np.savetxt('tmp_EGM08.dat',EGM08)
np.savetxt('tmp_sea_surface_slope.dat',sea_surface_slope)
np.savetxt('tmp_ssh.dat',sshao + msso)

#closing NC-file
nc_fid.close()
