#!/usr/bin/python

## IMPORTING PACKAGES
import sys
from netCDF4 import Dataset
from shutil import copyfile
import numpy as np

## READ NC-FILE FROM STDIN
file=sys.argv[1]
newfolder=sys.argv[2]

## COPY NC-FILE INTO NEW FOLDER
filename = file.split('/')[2]
newfile = newfolder+"/"+filename
copyfile(file,newfile)

## Open copied NC-file for reading and editing
nc_fid = Dataset(newfile,'r+');

## CREATE NEW VARIABLES and define DIMENSONS
sshv = nc_fid.createVariable("sea_surface_height","f8",("time",))
dtu15v = nc_fid.createVariable("dtu15mss_40hz","f8",("time","meas_ind",))
egm08v = nc_fid.createVariable("egm08_40hz","f8",("time","meas_ind",))
ssslopev = nc_fid.createVariable("sea_surface_slope_40hz","f8",("time","meas_ind",))

sshv.units = "[meters]"
dtu15v.units = "[meters]"
egm08v.units = "[meters]"
ssslopev.units = "[meters]"

sshv.description = "Mean Sea Surface Height. 1 Hz. Computed by DTU"
dtu15v.description = "DTU15 Mean Sea Surface. 40 Hz. Computed by DTU"
egm08v.description = "EGM08 geoid height. 40 Hz. Computed by DTU"
ssslopev.description = "Sea Surface Slope 40 Hz. (slope[i] (along track) = [ALTITUDE(i+1) - RANGE(i+1) - EGM08(i+1)] - [ALTITUDE(i-1) - RANGE(i-1) - EGM08(i-1)] ). Computed by DTU"

## LOAD data from ascii files
DTU15 = np.loadtxt('tmp_DTU15.dat')
EGM08 = np.loadtxt('tmp_EGM08.dat')
SSslope = np.loadtxt('tmp_sea_surface_slope.dat')
SSH = np.loadtxt('tmp_ssh.dat')

# Writing data to netcdf file
dtu15v[:] = DTU15
egm08v[:] = EGM08
ssslopev[:] = SSslope;
sshv[:] = SSH;

#closing NC-file
nc_fid.close()
