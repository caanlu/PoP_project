#!/usr/bin/python

## IMPORTING PACKAGES
import sys
import numpy as np

# Load Inverse SWH
Pointfile = np.loadtxt('tmp_pointfile.dat')
inverse_swh = (-1)*Pointfile[:,3]

onehz_length = len(inverse_swh)/40

inverse_swh = np.reshape(inverse_swh, [onehz_length,40])

# Saving into temporary ascii-files
np.savetxt('tmp_inverse_swh.dat',inverse_swh)

