#!/usr/bin/python

## IMPORTING PACKAGES
import sys
from netCDF4 import Dataset
from shutil import copyfile
import numpy as np

## READ FILE FROM STDIN
file=sys.argv[1]
newfolder=sys.argv[2]

## COPY FILE INTO NEW FOLDER
filename = file.split('/')[2]
newfile = newfolder+"/"+filename
copyfile(file,newfile)

## Open NC-file for reading and editing
nc_fid = Dataset(newfile,'r+');

## CREATE NEW VARIABLES and define DIMENSONS
inverse_swh = nc_fid.createVariable("inversed_swh_40hz","f8",("time","meas_ind"))
inverse_swh.units = "[meters]"

inverse_swh.description = "Inversed Significant Wave Height. 40 Hz. Computed by DTU"

## LOAD from files
InSWH = np.loadtxt('tmp_inverse_swh.dat')

# Writing data to netcdf file
inverse_swh[:] = InSWH

#closing NC-file
nc_fid.close()
