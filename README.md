GIT repos with python-scripts for reading NC-files (ORG_FILES) into GRAVSOFT-format and adding the variables back to the nc-file.

master.sh in the ORG_version folder reads it all (can only be run on DTU SPACE: geo5)

Example:
bash master.sh ../ORG_FILES OUT_FILES

test_master.sh in the TEST_version reads the test script, which should be able to run on any server with Python.

Example:
bash test_master.sh ../ORG_FILES OUT_FILES
